<!-- data-src="holder.js/900x500/auto/#777:#7a7a7a/text:First slide" -->


   <!--<div class="control-group" style="margin-top: 7%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Войти</button>
	        </div>
   </div>-->


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/logo2.jpg">

    <title>Спорт для всех</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="dist/css/carousel.css" rel="stylesheet">
    <link href="dist/css/jquery.fancybox.min.css" rel="stylesheet">
    <style>

    	textarea {
    		height: 300px;
    		max-width: 100%;
    		width: 50%;
    	}

        .thumb img {
            -webkit-filter: grayscale(0);
            filter: none;
            border-radius: 5px;
            background-color: #fff;
            border: 1px solid #ddd;
            padding: 5px;
        }

        .thumb img:hover {
            -webkit-filter: grayscale(1);
            filter: grayscale(1);
        }

        .thumb {
            padding: 5px;
        }

        .img-fluid {
		    max-width: 100%;
		    height: auto;
		}

		.collapse {
			display: none;
		}

		#form_singin {
			margin-left: 43%;
			margin-bottom: 5%;
		}

		#form_logout {
			margin-left: 53.5%;
			margin-bottom: 5%;
		}
		.controls {
			color: red;
		}

		#admin_menu {
			width: 29.9%; 
			margin: -0.8% 0 0 35.1%;
		}

		#admin_form {
			display: inline-block;
			position: absolute;
			margin: -0.05% 0 0 -2.1%;
		}

		.form-group {
			display: inline-block; 
			width: 77.2%;
		}

		#topic_form {
			margin-left: 37%;
		}
    </style>
  </head>
<!-- NAVBAR
================================================== -->
  <body>


	<?php  
		session_start();
		//unset($_SESSION['admin']);
		/*if(isset($_SESSION['nolog'])) { 
			unset($_SESSION['nolog']);
			$errorMessageHTML = '<div id="error_log"> 
									<span id="error_message"> 
								 		Неверный логин или пароль
								 	</span>
								</div>';
			$errorMessageStyle = '<style id="error_log_style">
							   		#error_log {
										display: block;
										position: absolute;
										margin: 43.5% 0 0 43.5%;
										background-color: red;
										text-align: center;
									}	
									#error_message {
										margin: 1;
										color: white;				
									}
							   </style>';
			$errorHideScript = '<script id="error_hide_script" type="text/javascript">
							   		setTimeout(function() {
							   			$("#error_log").remove();
							   			$("#error_log_style").remove();
							   			$("#error_hide_script").remove();
							   		}, 3000);
							   </script>';
			$logErrorContent = $errorMessageHTML . $errorMessageStyle . $errorHideScript;
			echo $logErrorContent;
		}*/
	?>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">	
        <div class="item active">
          <img alt="SPORT NMSK" src="assets/images/banner1.jpg">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>-->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/images/banner2.jpg">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>-->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/images/banner3.jpg">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>-->
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->


    <!--<form method="POST" id="form_upload" name="form" action="">
        <div class="control-group">
            <label class="control-label" for="login">Логин</label>
            <div class="controls">
              <input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">
            </div>
            <label class="control-label" for="password">Пароль</label>
            <div class="controls">
              <input type="text" id="password" name="password" class="password" value="" placeholder="Пароль">
            </div>
        </div>
        <div class="control-group" style="margin-top: 7%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Войти</button>
	        </div>
        </div>
    </form>-->

    <!--<form method="POST" id="form_upload" name="form" action="">
        <div class="control-group">
            <label class="control-label" for="title">Заголовок</label>
            <div class="controls">
              <input type="text" id="title" name="title" class="title" value="" placeholder="Заголовок">
            </div>
            <label class="control-label" for="content">Содержание</label>
            <div class="controls">
              <textarea id="content" name="content" class="content" value="" placeholder="Содержание">
              </textarea>
            </div>
            <label class="control-label" for="image">Изображение</label>
            <div class="controls" style="color: black">
              <input type="file" id="image" name="image" class="image">
            </div>
        </div>
        <div class="control-group" style="margin-top: 1%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Добавить</button>
	        </div>
        </div>
    </form>

    <div class="form-group" style="width: 21%">
	  <label for="option">Опции:</label>
	  <select class="form-control" id="option">
	    <option value="1">Добавить новость</option>
	    <option value="2">Добавить изображение в галерею</option>
	  </select>
	</div>-->

    <div class="wrapper">

	    
  	<div class="navbar-wrapper" style="position: relative;">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li id="about"><a href="javascript:void(0);">О нас</a></li>
                <li id="gallery"><a href="javascript:void(0);">Глерея</a></li>
                <li id="posts"><a href="javascript:void(0);">Новости</a></li>
                <li id="admin"><a href="javascript:void(0);">Админ</a></li>
                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Nav header</li>
                    <li><a href="#">Separated link</a></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>-->
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="container content">
    				<h1 class="my-4">О нас
    				</h1> 
				      <p>Мы являемся государственной организацией, которая занимается организацией и проведением спортивных мироприятий для общественности</p> 
				      <div class="row"> 
				        <div class="col-lg-12"> 
				          <h2 class="my-4">Наш коллектив</h2> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Дима депутат 
				            <small>начальник</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Сергей Продан 
				            <small>всемогущий</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Наталья Павловна 
				            <small>кто-то</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Неизвестный Дяда 
				            <small>бухгалтер</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				      </div>

    <?php if (isset($_GET['about'])): ?>
    	
    <?php elseif (isset($_GET['posts'])): ?>	
    	<!-- Three columns of text below the carousel -->
    	
	      <div class="row">
	        <div class="col-lg-4">
	          <img class="img-circle" alt="Generic placeholder image" src="assets/images/logo.jpg">
	          <h2>Heading</h2>
	          <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!-- /.col-lg-4 -->
	        <div class="col-lg-4">
	          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
	          <h2>Heading</h2>
	          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!-- /.col-lg-4 -->
	        <div class="col-lg-4">
	          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
	          <h2>Heading</h2>
	          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!-- /.col-lg-4 -->
	      </div><!-- /.row -->


   	<?php elseif (isset($_GET['gallery'])): ?> 
 
		    <h1 class="h3 text-center my-4">Галерея</h1>
		    <div class="row">
		        <div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/1.jpg">
		                <img class="img-fluid" src="assets/images/gallery/1.jpg" alt="...">
		            </a>
		        </div>
		        <div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/2.jpg">
		                <img class="img-fluid" src="assets/images/gallery/2.jpg" alt="...">
		            </a>
		        </div>
		       	<div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/3.jpg">
		                <img class="img-fluid" src="assets/images/gallery/3.jpg" alt="...">
		            </a>
		        </div>
		        <div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/4.jpg">
		                <img class="img-fluid" src="assets/images/gallery/4.jpg" alt="...">
		            </a>
		        </div>  
			    <div class="col-lg-3 col-md-4 col-6 thumb collapse">
			        <a data-fancybox="gallery" href="assets/images/gallery/5.jpg">
			            <img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">
			        </a>
			    </div> 
			    <div class="col-lg-3 col-md-4 col-6 thumb collapse">
			        <a data-fancybox="gallery" href="assets/images/gallery/5.jpg">
			            <img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">
			        </a>
			    </div>
		    </div>
			<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a> 
		
   	<?php elseif (isset($_GET['admin'])): ?>	
	     
	<?php endif; ?>

	      <!-- START THE FEATURETTES -->

	      <!--<hr class="featurette-divider">

	      <div class="row featurette">
	        <div class="col-md-7">
	          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
	          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
	        </div>
	        <div class="col-md-5">
	          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
	        </div>
	      </div>

	      <hr class="featurette-divider">

	      <div class="row featurette">
	        <div class="col-md-5">
	          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
	        </div>
	        <div class="col-md-7">
	          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
	          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
	        </div>
	      </div>

	      <hr class="featurette-divider">

	      <div class="row featurette">
	        <div class="col-md-7">
	          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
	          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
	        </div>
	        <div class="col-md-5">
	          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
	        </div>
	      </div>

	      <hr class="featurette-divider">-->

	      <!-- /END THE FEATURETTES -->

	         
	      <!-- FOOTER -->
	  
	      <footer>
	        <p class="pull-right"><a href="#">Back to top</a></p>
	        <p>&copy; 2018 Verba Ruslan &middot; </p>
	      </footer>
	    </div>
	</div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/docs.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
		$(".view.more").on("click", function() {
			$(".col-lg-3.col-md-4.col-6.thumb").toggleClass("collapse");
		});
	</script>
	<script type="text/javascript">

			 function changeSelect(option) { 
						         		var option = option.value;
						         		var content;
						         		switch (option) {
						         			case "1":
								         		content = '<div id="admin_menu">' +
													  '<div class="form-group">' +
														//'<label for="option">Опции:</label>' +
														'<select class="form-control" id="option" onchange="changeSelect(this)">' +
														   '<option value="1"> Добавить новость</option>' +
														   '<option value="2"> Добавить изображение в галерею</option>' +
														'</select>' +
													  '</div>' +
													  '<div id="admin_form" class="form">' +
														'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
														      '<div class="control-group">' +
														        '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
															        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
															    '</div>' +
														    '</div>' +
														'</form>' +
													  '</div>' +
												  '</div>' +
												  first_option_content +
											      footer;
						         				break;
						         			case "2":
							         			content = '<div id="admin_menu">' +
												  '<div class="form-group">' +
													//'<label for="option">Опции:</label>' +
													'<select class="form-control" id="option" onchange="changeSelect(this)">' +
													   '<option value="1"> Добавить новость</option>' +
													   '<option value="2"> Добавить изображение в галерею</option>' +
													'</select>' +
												  '</div>' +
												  '<div id="admin_form" class="form">' +
													'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
													      '<div class="control-group">' +
													        '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
														        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
														    '</div>' +
													    '</div>' +
													'</form>' +
												  '</div>' +
											  '</div>' +
											  two_option_content +
										      footer;
						         			break;
						         		} 
						         		el.get(0).innerHTML = content; 
						         		$('#option').val(option);	
						         	}
		var singin = false;
		var el = $('.container.content');
		var footer = '<footer>' +
					    '<p class="pull-right"><a href="#">Back to top</a></p>' +
					    '<p>&copy; 2018 Verba Ruslan &middot; </p>' +
					 '</footer>';

		$('#about').on('click', function() {

		    var content = '<h1 class="my-4">О нас' +
					      '</h1>' +
					      '<p>Мы являемся государственной организацией, которая занимается организацией и проведением спортивных мироприятий для общественности</p>' +
					      '<div class="row">' +
					        '<div class="col-lg-12">' +
					          '<h2 class="my-4">Наш коллектив</h2>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Дима депутат' +
					            ' <small>начальник</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Сергей Продан' +
					            ' <small>всемогущий</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Наталья Павловна' +
					            ' <small>кто-то</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Неизвестный Дяда' +
					            ' <small>бухгалтер</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					      '</div>' +
					      footer;  			
			el.get(0).innerHTML = content;
		});

		$('#gallery').on('click', function() {

		    var content = '<h1 class="h3 text-center my-4">Галерея</h1>' +
						    '<div class="row">' +
						        '<div class="col-lg-3 col-md-4 col-6 thumb">' +
						            '<a data-fancybox="gallery" href="assets/images/gallery/1.jpg">' +
						                '<img class="img-fluid" src="assets/images/gallery/1.jpg" alt="...">' +
						            '</a>' +
						        '</div>' +
						        '<div class="col-lg-3 col-md-4 col-6 thumb">' +
						            '<a data-fancybox="gallery" href="assets/images/gallery/2.jpg">' +
						                '<img class="img-fluid" src="assets/images/gallery/2.jpg" alt="...">' +
						            '</a>' +
						        '</div>' +
						       	'<div class="col-lg-3 col-md-4 col-6 thumb">' +
						            '<a data-fancybox="gallery" href="assets/images/gallery/3.jpg">' +
						                '<img class="img-fluid" src="assets/images/gallery/3.jpg" alt="...">' +
						            '</a>' +
						        '</div>' +
						        '<div class="col-lg-3 col-md-4 col-6 thumb">' +
						            '<a data-fancybox="gallery" href="assets/images/gallery/4.jpg">' +
						                '<img class="img-fluid" src="assets/images/gallery/4.jpg" alt="...">' +
						            '</a>' +
						        '</div>' +  
							    '<div class="col-lg-3 col-md-4 col-6 thumb collapse">' +
							        '<a data-fancybox="gallery" href="assets/images/gallery/5.jpg">' +
							            '<img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">' +
							        '</a>' +
							    '</div>' + 
							    '<div class="col-lg-3 col-md-4 col-6 thumb collapse">' +
							        '<a data-fancybox="gallery" href="assets/images/gallery/5.jpg">' +
							            '<img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">' +
							        '</a>' +
							    '</div>' +
						    '</div>' +
							'<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a>' + 
					      footer;  			
			el.get(0).innerHTML = content;
		});

								    var	first_option_content = '<div id="topic_form" class="form">' +
						        						'<form method="POST" id="form_upload" name="form" action="">' +
													        '<div class="control-group">' +
													            '<label class="control-label" for="title">Заголовок</label>' +
													            '<div class="controls">' +
													              '<input type="text" id="title" name="title" class="title" value="" placeholder="Заголовок">' +
													            '</div>' +
													            '<label class="control-label" for="content">Содержание</label>' +
													            '<div class="controls">' +
													              '<textarea id="content" name="content" class="content" value="" placeholder="Содержание">' +
													              '</textarea>' +
													            '</div>' +
													            '<label class="control-label" for="image">Изображение</label>' +
													            '<div class="controls" style="color: black">' +
													              '<input type="file" id="image" name="image" class="image">' +
													            '</div>' +
													        '</div>' +
													        '<div class="control-group" style="margin-top: 1%">' +
														        '<div class="controls">' + 
														            '<button type="submit" name="submit" class="btn btn-success">Добавить</button>' +
														        '</div>' +
													        '</div>' +
														'</form>' +
													  '</div>';	


													  var	two_option_content = 'gav';	

		//$('#form_singin').on('click', call_singin());
		$('#admin').on('click', function() { 
			$.ajax({
					type: "POST",
					url: "access_admin.php",
					success: function(data) {  
						if (data) {
							var content = '<div id="admin_menu">' +
											  '<div class="form-group">' +
												//'<label for="option">Опции:</label>' +
												'<select class="form-control" id="option" onchange="changeSelect(this)">' +
												   '<option value="1"> Добавить новость</option>' +
												   '<option value="2"> Добавить изображение в галерею</option>' +
												'</select>' +
											  '</div>' +
											  '<div id="admin_form" class="form">' +
												'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
												      '<div class="control-group">' +
												        '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
													        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
													    '</div>' +
												    '</div>' +
												'</form>' +
											  '</div>' +
										  '</div>' +
										  first_option_content +
									      footer;	  
						} else {
							var content = '<div class="form">' +
											'<form method="POST" id="form_singin" name="form" action="javascript:void(null);" onclick="call_singin()">' +
											    '<div class="control-group">' +
											        '<label class="control-label" for="login">Логин</label>' +
											        '<div class="controls">' +
											            '<input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">' +
											        '</div>' +
											        '<label class="control-label" for="password">Пароль</label>' +
											        '<div class="controls">' +
											            '<input type="text" id="password" name="password" class="password" value="" placeholder="Пароль администратора">' +
											        '</div>' +
											        '<div class="controls" style="margin-top: 2%"> ' +
												        '<button type="submit" name="submit" class="btn btn-success">Войти</button>' +
												    '</div>' +
											    '</div>' +
											'</form>' +
										  '</div>' +
										  footer;
						}
						el.get(0).innerHTML = content;
					},
					error:  function(xhr, str) {

					}
			});	
		});


				function call_singin() {
									$("#form_singin").validate({
							              errorElement: "div",
							              rules: {
							                login: "required",
							                password: "required",
							              },
							              messages: {
							                login: "Введите логин",
							                password: "Введите пароль",
							              },
							              submitHandler: function(form) { 
							                var msg = $("#form_singin").serialize(); 
							                $.ajax({
							                  type: "POST",
							                  url: "access_admin.php",
							                  data: msg,
							                  success: function(data) { 
								                  		if (data) {
								         				         		var content =  '<div class="form">' +
																			'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
																		        '<div class="control-group">' +
																		            '<div class="controls">' +
																		              '<input type="text" id="logout" name="logout" class="logout" value="" style="display: none">' +
																		            '</div>' +
																		            '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
																			            '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' + 
																			        '</div>' +
																		        '</div>' +
																		    '</form>' +
																	    '</div>' +
																	    footer; 
														} else {
															 var errorMessageStyleContent = '#error_log {' +
																						    'display: block;' +
																						    'position: absolute;' +
																						    'margin: 9% 0 5% 43.5%;' +
																						    'background-color: red;' +
																						    'text-align: center;' +
																					    '}' +	
																					    '#error_message {' +
																						    'margin: 1;' +
																						    'color: white;' +				
																					    '};';
														var errorMessageStyle = document.createElement("style"); 
														errorMessageStyle.innerHTML = errorMessageStyleContent;
														var errorHideScriptContent = setTimeout(function() {
																			   			  $("#error_log").remove();
																			   			  $("#error_log_style").remove();
																			   			  $("#error_hide_script").remove();
																		   			  }, 3000);
														var errorHideScript = document.createElement("script");
														errorHideScript.innerHTML = errorHideScriptContent;
														var head = $("head")[0];
														head.append(errorMessageStyle);
														head.append(errorHideScript);
														var content = '<div id="error_log">' + 
																					'<span id="error_message">' +  
																				 		'Неверный логин или пароль' + 
																				 	'</span>' + 
																				'</div>' +				   	   
														'<div class="form">' +
															'<form method="POST" id="form_singin" name="form" action="javascript:void(null);" onclick="call_singin()">' +
														        '<div class="control-group">' +
														            '<label class="control-label" for="login">Логин</label>' +
														            '<div class="controls">' +
														              '<input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">' +
														            '</div>' +
														            '<label class="control-label" for="password">Пароль</label>' +
														            '<div class="controls">' +
														              '<input type="text" id="password" name="password" class="password" value="" placeholder="Пароль администратора">' +
														            '</div>' +
														            '<div class="controls" style="margin-top: 2%"> ' +
															            '<button type="submit" name="submit" class="btn btn-success">Войти</button>' +
															        '</div>' +
														        '</div>' +
														    '</form>' +
													    '</div>' +
													    footer;
														}
													el.get(0).innerHTML = content;
							                  },
							                  error:  function(xhr, str){

							                  }
							                });
							              }
						            });
						         };

		function call_logout() { 
									$("#form_logout").validate({ 
							              errorElement: "div", 
							              rules: { 
							                login: "required", 
							                password: "required", 
							              }, 
							              messages: { 
							                login: "Введите логин", 
							                password: "Введите пароль", 
							              }, 
							              submitHandler: function(form) {  
							                $.ajax({ 
							                  type: "POST", 
							                  url: "access_admin.php", 
							                  data: "logout", 
							                  success: function(data) { 
							                  		if (!data) {   				   	   
														var content = '<div class="form">' +
																			  '<form method="POST" id="form_singin" name="form" action="javascript:void(null);" onclick="call_singin()">' +
																		          '<div class="control-group">' +
																		              '<label class="control-label" for="login">Логин</label>' +
																		              '<div class="controls">' +
																		                '<input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">' +
																		              '</div>' +
																		              '<label class="control-label" for="password">Пароль</label>' +
																		              '<div class="controls">' +
																		                '<input type="text" id="password"   name="password" class="password" value="" placeholder="Пароль администратора">' +
																		              '</div>' +
																		              '<div class="controls" style="margin-top: 2%"> ' +
																			              '<button type="submit" name="submit"   class="btn btn-success">Войти</button>' +
																			          '</div>' +
																		          '</div>' +
																		      '</form>' +
																	      '</div>' +
																	      footer; 
														} else {
															var content = '<div class="form">' +
																		   '<form method="POST" id="form_logout" name="form" action="javascript:void(null);" action="call_logout()">' +
																		       '<div class="control-group">' +
																		           '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
																			           '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' + 
																			       '</div>' +
																		       '</div>' +
																		   '</form>' +
																	   '</div>' +
																	   footer; 
														}
													el.get(0).innerHTML = content; 
							                  }, 
							                  error:  function(xhr, str){ 
							              	  	
							                  } 
							                }); 
							              } 
						            }); 
						         };
						  
						         //});
	</script>
  </body>
</html>