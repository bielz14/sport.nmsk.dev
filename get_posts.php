<?php require "db_connect.php"; ?>
<?php 
	if ((isset($_POST['posts']) && $_POST['posts']) || isset($_GET['page'])) {
		$connect = mysqli_connect('localhost', 'root', '', 'sport');
		$query = "SELECT * FROM `post`";
		$posts = [];
		if ($result = mysqli_query($connect, $query)) {
			while ($row = mysqli_fetch_assoc($result)) {
				$currentDate = strtotime(date('Y-m-d H:i:s'));
				$date = $currentDate - strtotime($row['date_of_added']);
				$date = date('j', $date) . ' д. ' . date('G', $date) . ' ч';
				array_push($posts, ['title' => $row['title'], 'content' => $row['content'], 'image' => $row['image'], 'date' => $date]);
			}
		}
		if (!isset($_GET['page'])) {
			$startItem = 1;
		} else {
			$startItem = ($_GET['page'] - 1) * 6 ;
		}
		$countAll = count($posts);
		$posts = array_slice($posts, $startItem, 6);
		if (count($posts)) {
			echo json_encode(['count_all' => $countAll,'posts_page' => $posts]);
			return;
		}
	}
	echo 0;
?>