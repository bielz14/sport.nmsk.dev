<?php require "db_connect.php"; ?>
<?php 
	if ((isset($_POST['gallery']) && $_POST['gallery']) || isset($_GET['page'])) {
		$connect = mysqli_connect('localhost', 'root', '', 'sport');
		$query = "SELECT * FROM `image`";
		$images = [];
		if ($result = mysqli_query($connect, $query)) {
			while ($row = mysqli_fetch_assoc($result)) {
				array_push($images, ['path' => $row['path'], 'description' => $row['description']]);
			}
		}
		if (!isset($_GET['page'])) {
			$startItem = 1;
		} else {
			$startItem = ($_GET['page'] - 1) * 6;
		}
		$countAll = count($images);
		$images = array_slice($images, $startItem, 6);
		if (count($images)) {
			echo json_encode(['count_all' => $countAll,'images_page' => $images]);
			return;
		}
	}
	echo 0;
?>