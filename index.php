<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/logo2.jpg">

    <title>Спорт для всех</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="dist/css/carousel.css" rel="stylesheet">
    <link href="dist/css/jquery.fancybox.min.css" rel="stylesheet">

    <style>

    .view-more-images a img:hover {
        background: url(/assets/images/button.gif);
        transform: rotate(380deg);
        background-size: cover
    }

    .view-more-images a img {
        width: 25px;
        height: 25px;
        background: url(/assets/images/rotateblack.gif);
        background-size: cover;
    }

    .view-more-posts a img:hover {
        background: url(/assets/images/button.gif);
        transform: rotate(380deg);
        background-size: cover
    }

    .view-more-posts a img {
        width: 25px;
        height: 25px;
        background: url(/assets/images/rotateblack.gif);
        background-size: cover;
    }

      body, html {
        height: 100%;
      }

    	textarea#content {
    		height: 300px;
    		max-width: 100%;
    		width: 47.5%;
    	}

      textarea#description {
        height: 100px;
        max-width: 100%;
        width: 47.5%;
        resize: none;
      }

		.collapse {
			display: none;
		}

		#form_singin {
			margin-left: 43%;
			margin-bottom: 5%;
		}

		#form_logout {
			margin-left: 53.5%;
			margin-bottom: 5%;
		}

		.error {
			color: red;
		}

		#image {
			color: black;
		}

		#admin_menu {
			width: 29.9%; 
			margin: -0.8% 0 0 37%;
		}

		#admin_form {
			display: inline-block;
			position: absolute;
			margin: -0.05% 0 0 -2.1%;
		}

		.form-group {
			display: inline-block; 
			width: 77.2%;
		}

		#topic_form {
			margin-left: 37%;
		}

		.post_message {
			position: absolute; 
			margin: 1% 0 0 15%;
			padding: 1%;
			display: none;
		}

    .image_message {
      position: absolute; 
      margin: 1% 0 0 19%;
      padding: 1%;
      display: none;
    }

		@media (min-width: 1200px) {
			.col-lg-3 {
			    width: 30%;
			}
		}

    .not_image {
      width: 28.1%;
      height: 10%;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      margin: auto;
      margin-bottom: 14%;
      background: linear-gradient(to top left, purple, crimson, orangered, gold);
      /*background: orange;*/
      color: white;
      padding: 2.5%;
      border-radius: 50%;
      font-size: 120%;
      font-family:'Bradley Hand', cursive;
    }

    .not_posts {
      width: 28.1%;
      height: 10%;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      margin: auto;
      margin-bottom: 14%;
      background: linear-gradient(to top left, purple, crimson, orangered, gold);
      /*background: orange;*/
      color: white;
      padding: 2.5%;
      border-radius: 50%;
      font-size: 120%;
      font-family:'Bradley Hand', cursive;
    }

    .img-fluid {
      max-width: 100%;
      height: 100%;
      display: block;
      margin-left: auto;
      margin-right: auto;
    }

    .thumb {
      background: gray;
      border: 1px solid #ddd;
      border-radius: 5px;
      width: 40%;
      height: 30%;
      float: left;
      margin: 2%;

      max-width: 450px; /* опционально */
      opacity: .99;
      overflow: hidden;
      position: relative;
      -webkit-box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);
      -moz-box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);
      box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);
    }

    .thumb:before {
      content: '';
      background: -webkit-linear-gradient(top, transparent 0%, rgba(0, 0, 0, 0.7) 100%);
      background: linear-gradient(to bottom, transparent 0%, rgba(0, 0, 0, 0.7) 100%);
      width: 100%;
      height: 50%;
      opacity: 0;
      position: absolute;
      top: 100%;
      left: 0;
      z-index: 2;
      -webkit-transition-property: top, opacity;
      transition-property: top, opacity;
      -webkit-transition-duration: 0.3s;
      transition-duration: 0.3s;
    }

    .thumb .caption {
      width: 100%;
      padding: 20px;
      color: #fff;
      position: absolute;
      bottom: 0;
      left: 0;
      z-index: 3;
      text-align: center;
    }
    .thumb .caption span {
      display: block;
      opacity: 0;
      position: relative;
      top: 100px;
      -webkit-transition-property: top, opacity;
      transition-property: top, opacity;
      -webkit-transition-duration: 0.3s;
      transition-duration: 0.3s;
      -webkit-transition-delay: 0s;
      transition-delay: 0s;
    }
    .thumb .caption .title {
      line-height: 1;
      font-weight: normal;
      font-size: 18px;
    }
    .thumb .caption .info {
      line-height: 1.2;
      margin-top: 5px;
      font-size: 12px;
    }
    .thumb:focus:before,
    .thumb:focus span, .thumb:hover:before,
    .thumb:hover span {
      opacity: 1;
    }
    .thumb:focus:before, .thumb:hover:before {
      top: 50%;
    }
    .thumb:focus span, .thumb:hover span {
      top: 0;
    }
    .thumb:focus .title, .thumb:hover .title {
      -webkit-transition-delay: 0.15s;
              transition-delay: 0.15s;
    }
    .thumb:focus .info, .thumb:hover .info {
      -webkit-transition-delay: 0.25s;
              transition-delay: 0.25s;
    }

    .row_about {
      width: 100%;
      margin-left: -15px;
      margin-right: -15px;
    }

    .row_posts {
      width: 100%;
      margin-bottom: 3%;
      padding-left: 7%;
    }

    .row {
      width: 100%;
      height: 700px;
      margin-bottom: 10%;
      padding-left: 12%;
    }

    .pagination_gallery {
      display: inline-block;
      margin: 0 30% 0 30%;
    }

    .pagination_gallery a {
      color: black;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
    }

    .pagination_gallery a.active {
      background-color: #4CAF50;
      color: white;
      border-radius: 5px;
    }

    .pagination_gallery a:hover:not(.active) {
      background-color: #ddd;
      border-radius: 5px;
    }

    .pagination_posts {
      display: inline-block;
      margin: 0 30% 0 30%;
    }

    .pagination_posts a {
      color: black;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
    }

    .pagination_posts a.active {
      background-color: #4CAF50;
      color: white;
      border-radius: 5px;
    }

    .pagination_posts a:hover:not(.active) {
      background-color: #ddd;
      border-radius: 5px;
    }
    </style>
  </head>
<!-- NAVBAR
================================================== -->
  <body>


	<?php  
		//session_start();
		//unset($_SESSION['admin']);
		/*if(isset($_SESSION['nolog'])) { 
			unset($_SESSION['nolog']);
			$errorMessageHTML = '<div id="error_log"> 
									<span id="error_message"> 
								 		Неверный логин или пароль
								 	</span>
								</div>';
			$errorMessageStyle = '<style id="error_log_style">
							   		#error_log {
										display: block;
										position: absolute;
										margin: 43.5% 0 0 43.5%;
										background-color: red;
										text-align: center;
									}	
									#error_message {
										margin: 1;
										color: white;				
									}
							   </style>';
			$errorHideScript = '<script id="error_hide_script" type="text/javascript">
							   		setTimeout(function() {
							   			$("#error_log").remove();
							   			$("#error_log_style").remove();
							   			$("#error_hide_script").remove();
							   		}, 3000);
							   </script>';
			$logErrorContent = $errorMessageHTML . $errorMessageStyle . $errorHideScript;
			echo $logErrorContent;
		}*/
	?>
    
    <header>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 0">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">	
        <div class="item active">
          <img alt="SPORT NMSK" src="assets/images/banner1.jpg">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>-->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/images/banner2.jpg">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>-->
            </div>
          </div>
        </div>
        <div class="item">
          <img src="assets/images/banner3.jpg">
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>-->
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->


    <!--<form method="POST" id="form_upload_post" name="form" action="">
        <div class="control-group">
            <label class="control-label" for="login">Логин</label>
            <div class="controls">
              <input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">
            </div>
            <label class="control-label" for="password">Пароль</label>
            <div class="controls">
              <input type="text" id="password" name="password" class="password" value="" placeholder="Пароль">
            </div>
        </div>
        <div class="control-group" style="margin-top: 7%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Войти</button>
	        </div>
        </div>
    </form>-->

    <!--<form method="POST" id="form_upload_post" name="form" action="">
        <div class="control-group">
            <label class="control-label" for="title">Заголовок</label>
            <div class="controls">
              <input type="text" id="title" name="title" class="title" value="" placeholder="Заголовок">
            </div>
            <label class="control-label" for="content">Содержание</label>
            <div class="controls">
              <textarea id="content" name="content" class="content" value="" placeholder="Содержание">
              </textarea>
            </div>
            <label class="control-label" for="image">Изображение</label>
            <div class="controls" style="color: black">
              <input type="file" id="image" name="image" class="image">
            </div>
        </div>
        <div class="control-group" style="margin-top: 1%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Добавить</button>
	        </div>
        </div>
    </form>

    <div class="form-group" style="width: 21%">
	  <label for="option">Опции:</label>
	  <select class="form-control" id="option">
	    <option value="1">Добавить новость</option>
	    <option value="2">Добавить изображение в галерею</option>
	  </select>
	</div>-->
	<nav id="navigation" class="navbar navbar-default navbar-top">
        <div class="container">
                <button class="navbar-toggle" type="button" data-target="#navbar-collapse" data-toggle="collapse">
                    <span class="sr-only">Меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/" style="float: none">
                    <img id="logo" src="assets/ico/logo2.jpg" alt="Логотип" width="2%" height="100%" style="padding-top: 1%;">
                </a>
            <div id="navbar-collapse" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
					<li id="about"><a href="javascript:void(0);">О нас</a></li>
	                <li id="gallery"><a href="javascript:void(0);">Галерея</a></li>
	                <li id="posts"><a href="javascript:void(0);">Блог</a></li>
	                <li id="admin"><a href="javascript:void(0);">Админ</a></li>
                </ul>
            </div>
        </div>
    </nav>
		<!--<div class="navbar navbar-inverse navbar-static-top adapter" role="navigation">
          <div class="container">
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li id="about"><a href="javascript:void(0);">О нас</a></li>
                <li id="gallery"><a href="javascript:void(0);">Глерея</a></li>
                <li id="posts"><a href="javascript:void(0);">Блог</a></li>
                <li id="admin"><a href="javascript:void(0);">Админ</a></li>
              </ul>
            </div>
          </div>
        </div>-->
    </header>


    <!--<form method="POST" id="form_upload_post" name="form" action="">
        <div class="control-group">
            <label class="control-label" for="login">Логин</label>
            <div class="controls">
              <input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">
            </div>
            <label class="control-label" for="password">Пароль</label>
            <div class="controls">
              <input type="text" id="password" name="password" class="password" value="" placeholder="Пароль">
            </div>
        </div>
        <div class="control-group" style="margin-top: 7%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Войти</button>
	        </div>
        </div>
    </form>-->

    <!--<form method="POST" id="form_upload_post" name="form" action="">
        <div class="control-group">
            <label class="control-label" for="title">Заголовок</label>
            <div class="controls">
              <input type="text" id="title" name="title" class="title" value="" placeholder="Заголовок">
            </div>
            <label class="control-label" for="content">Содержание</label>
            <div class="controls">
              <textarea id="content" name="content" class="content" value="" placeholder="Содержание">
              </textarea>
            </div>
            <label class="control-label" for="image">Изображение</label>
            <div class="controls" style="color: black">
              <input type="file" id="image" name="image" class="image">
            </div>
        </div>
        <div class="control-group" style="margin-top: 1%">
	        <div class="controls"> 
	            <button type="submit" name="submit" class="btn btn-success">Добавить</button>
	        </div>
        </div>
    </form>

    <div class="form-group" style="width: 21%">
	  <label for="option">Опции:</label>
	  <select class="form-control" id="option">
	    <option value="1">Добавить новость</option>
	    <option value="2">Добавить изображение в галерею</option>
	  </select>
	</div>-->


    <div class="wrapper">

	    
  	<!--<div class="navbar-wrapper" style="position: relative;">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-collapse collapse">
              <div id="navbar-collapse" class="collapse navbar-collapse navbar-right">
                <li id="about"><a href="javascript:void(0);">О нас</a></li>
                <li id="gallery"><a href="javascript:void(0);">Галерея</a></li>
                <li id="posts"><a href="javascript:void(0);">Блог</a></li>
                <li id="admin"><a href="javascript:void(0);">Админ</a></li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>-->

    <div class="container content">
    				    				<h1 class="my-4">О нас
    				</h1> 
				      <p>Мы являемся государственной организацией, которая занимается организацией и проведением спортивных мероприятий для общественности</p> 
				      <div class="row_about"> 
				        <div class="col-lg-12" style="margin-bottom: 2%"> 
				          <h2 class="my-4">Наш коллектив</h2> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Жадан 
				            <small>начальник</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Сергей Продан 
				            <small>всемогущий</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Наталья Павловна 
				            <small>кто-то</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				        <div class="col-lg-4 col-sm-6 text-center mb-4"> 
				          <img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt=""> 
				          <h3>Неизвестный Дяда 
				            <small>бухгалтер</small> 
				          </h3> 
				          <p>What does this team member to? Keep it short! This is also a great spot for social links!</p> 
				        </div> 
				      </div>

    <?php if (isset($_GET['about'])): ?>
    	
    <?php elseif (isset($_GET['posts'])): ?>	
    	<!-- Three columns of text below the carousel -->
    	
	      <div class="row">
	        <div class="col-lg-4">
	          <img class="img-circle" alt="Generic placeholder image" src="assets/images/logo.jpg">
	          <h2>Heading</h2>
	          <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!-- /.col-lg-4 -->
	        <div class="col-lg-4">
	          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
	          <h2>Heading</h2>
	          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!-- /.col-lg-4 -->
	        <div class="col-lg-4">
	          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
	          <h2>Heading</h2>
	          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!-- /.col-lg-4 -->
	      </div><!-- /.row -->


   	<?php elseif (isset($_GET['gallery'])): ?> 
 
		    <h1 class="h3 text-center my-4">Галерея</h1>
		    <div class="row">
		        <div class="test">
		            <a data-fancybox="gallery" href="assets/images/gallery/1.jpg">
		                <img class="img-fluid" src="assets/images/gallery/1.jpg" alt="...">
		            </a>
		        </div>
		        <div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/2.jpg">
		                <img class="img-fluid" src="assets/images/gallery/2.jpg" alt="...">
		            </a>
		        </div>
		       	<div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/3.jpg">
		                <img class="img-fluid" src="assets/images/gallery/3.jpg" alt="...">
		            </a>
		        </div>
		        <div class="col-lg-3 col-md-4 col-6 thumb">
		            <a data-fancybox="gallery" href="assets/images/gallery/4.jpg">
		                <img class="img-fluid" src="assets/images/gallery/4.jpg" alt="...">
		            </a>
		        </div>  
			    <div class="col-lg-3 col-md-4 col-6 thumb collapse">
			        <a data-fancybox="gallery" href="assets/images/gallery/5.jpg">
			            <img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">
			        </a>
			    </div> 
			    <div class="col-lg-3 col-md-4 col-6 thumb collapse">
			        <a data-fancybox="gallery" href="assets/images/gallery/5.jpg">
			            <img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">
			        </a>
			    </div>
		    </div>
			<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a> 
		
   	<?php elseif (isset($_GET['admin'])): ?>	
	     
	<?php endif; ?>
	      <!-- START THE FEATURETTES -->

	      <!--<hr class="featurette-divider">

	      <div class="row featurette">
	        <div class="col-md-7">
	          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
	          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
	        </div>
	        <div class="col-md-5">
	          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
	        </div>
	      </div>

	      <hr class="featurette-divider">

	      <div class="row featurette">
	        <div class="col-md-5">
	          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
	        </div>
	        <div class="col-md-7">
	          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
	          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
	        </div>
	      </div>

	      <hr class="featurette-divider">

	      <div class="row featurette">
	        <div class="col-md-7">
	          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
	          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
	        </div>
	        <div class="col-md-5">
	          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
	        </div>
	      </div>

	      <hr class="featurette-divider">-->

	      <!-- /END THE FEATURETTES -->

	         
	      <!-- FOOTER -->
	    </div>
	</div>
  <footer style="margin-top: 10%">
      <p class="pull-right" style="margin-right: 7%"><a href="#">Back to top</a></p>
      <p style="margin: 3% 0 2% 7%">&copy; 2019 Verba Ruslan &middot; </p>
  </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/docs.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
		$(".view.more").on("click", function() {
			$(".col-lg-3.col-md-4.col-6.thumb").toggleClass("collapse");
		});
	</script>
	<script type="text/javascript">

			 function changeSelect(option) { 
						         		var option = option.value;
						         		var content;
						         		switch (option) {
						         			case "1":
								         		content = '<div id="admin_menu">' +
													  '<div class="form-group">' +
														//'<label for="option">Опции:</label>' +
														'<select class="form-control" id="option" onchange="changeSelect(this)">' +
														   '<option value="1"> Добавить новость</option>' +
														   '<option value="2"> Добавить изображение в галерею</option>' +
														'</select>' +
													  '</div>' +
													  '<div id="admin_form" class="form">' +
														'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
														      '<div class="control-group">' +
														        '<div class="controls"> ' +
															        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
															    '</div>' +
														    '</div>' +
														'</form>' +
													  '</div>' +
												  '</div>' +
												  first_option_content;// +
											      //footer;
						         				break;
						         			case "2":
							         			content = '<div id="admin_menu">' +
												  '<div class="form-group">' +
													//'<label for="option">Опции:</label>' +
													'<select class="form-control" id="option" onchange="changeSelect(this)">' +
													   '<option value="1"> Добавить новость</option>' +
													   '<option value="2"> Добавить изображение в галерею</option>' +
													'</select>' +
												  '</div>' +
												  '<div id="admin_form" class="form">' +
													'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
													      '<div class="control-group">' +
													        '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
														        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
														    '</div>' +
													    '</div>' +
													'</form>' +
												  '</div>' +
											  '</div>' +
											  two_option_content;// +
										      //footer;
						         			break;
						         		} 
						         		el.get(0).innerHTML = content; 
						         		$('#option').val(option);	
						         	}
		var singin = false;
		var el = $('.container.content');
		var footer = '<footer>' +
					    '<p class="pull-right"><a href="#">Back to top</a></p>' +
					    '<p>&copy; 2018 Verba Ruslan &middot; </p>' +
					 '</footer>';

		$('#about').on('click', function() {

		    var content = '<h1 class="my-4">О нас' +
					      '</h1>' +
					      '<p>Мы являемся государственной организацией, которая занимается организацией и проведением спортивных мероприятий для общественности</p>' +
					      '<div class="row_about">' +
					        '<div class="col-lg-12" style="margin-bottom: 2%">' +
					          '<h2 class="my-4">Наш коллектив</h2>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Жадан' +
					            ' <small>начальник</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Сергей Продан' +
					            ' <small>всемогущий</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Наталья Павловна' +
					            ' <small>кто-то</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					        '<div class="col-lg-4 col-sm-6 text-center mb-4">' +
					          '<img class="img-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">' +
					          '<h3>Неизвестный Дяда' +
					            ' <small>бухгалтер</small>' +
					          '</h3>' +
					          '<p>What does this team member to? Keep it short! This is also a great spot for social links!</p>' +
					        '</div>' +
					      '</div>';// +
					      //footer;  			
			el.get(0).innerHTML = content;
			$([document.documentElement, document.body]).animate({
		        scrollTop: $(".content").offset().top
		    }, 1000);
		});

		$('#gallery').on('click', function() {
              var content;

              $.ajax({
                    type: "POST",
                    url: "get_images.php",
                    data: {'gallery': true},
                    dataType: 'json',
                    success: function(data) {
                      //content = '<h1 class="h3 text-center my-4">Галерея</h1>';
                      if (!data['images_page']) {
                        content = 
                            '<div class="not_image">' +
                              '<span>Изображения отсутствуют в галереи</span>' +
                            '</div>';
                      } else {
                          content = '<h1 class="h3 text-center my-4">Галерея</h1>';
                          content += '<div class="row">';             
                          $.each(data['images_page'], function(key, value) {
                            content += 
                                '<div class="thumb">' +
                                  '<a data-fancybox="gallery" href="' + value.path + '">' +
                                    '<img class="img-fluid" src="' + value.path + '" alt="' + value.description + '">' +
                                  '</a>' +
                                    '<div class="caption">' +
                                        '<span class="info">' + value.description + '</span>' +
                                    '</div>' +
                                '</div>';
                          });
                          var pageCount = Math.ceil(data['count_all'] / 6);
                          if (pageCount > 1) {
                            content += '<div class="pagination_gallery">';
                                        //'<a href="http://sport.nmsk2/#">&laquo;</a>';
                            for (var i = 1; i <= pageCount; i++) {
                              if (i == 1) {
                                content += '<a href="#" class="active" data-type="num">' + i + '</a>';
                              } else {
                                content += '<a href="#" data-type="num">' + i + '</a>';
                              }
                            }                          
                            content += '<a class="next" href="#">&raquo;</a>' +
                                      '</div>';
                          }
                          content += '<div class="view-more-images" style="margin-left: auto; width: 300px; margin-right: auto; margin-top: 1%; display: block" data-counter="1">' +
                                        '<a href="#" onmouseover="this.style.textDecoration=\'none\'; this.style.color = \'#428bca\'">' +
                                          '<img src="assets/images/more.gif" style="margin-left: 10%">' +
                                          '<span style="display: block;">Показать ещё</span>' +
                                        '</a>' +
                                     '</div>' +
                                  '</div>';
                          /*content += '<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a>';
                          content += footer;*/
                          /*content = '<h1 class="h3 text-center my-4">Галерея</h1>' +
                            '<div class="row">' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/1.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/1.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/2.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/2.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/3.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/3.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/4.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/4.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +  
                              '<div class="col-lg-3 col-md-4 col-6 thumb collapse">' +
                                  '<a data-fancybox="gallery" href="assets/images/gallery/5.jpg">' +
                                      '<img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">' +
                                  '</a>' +
                              '</div>' + 
                              '<div class="col-lg-3 col-md-4 col-6 thumb collapse">' +
                                  '<a data-fancybox="gallery" href="assets/images/gallery/5.jpg">' +
                                      '<img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">' +
                                  '</a>' +
                              '</div>' +
                            '</div>' +
                          '<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a>' + 
                            footer;*/
                      }

                      el.get(0).innerHTML = content;
                      $([document.documentElement, document.body]).animate({
                            scrollTop: $(".content").offset().top
                      }, 1000);
                    },
                    error: function(xhr, str) {
                     
                    }
              });
		});
    
    
    $('body').on('mouseover', '.view-more-images', function() {
      $('.view-more-images a img').css({
        'background' : 'url(/assets/images/button.gif)',
        //'transform' : 'rotate(380deg)',
        'background-size' : 'cover'
      });
    });

    $('body').on('mouseout', '.view-more-images', function() {
      $('.view-more-images a img').css({
        //'width' : '25px',
        //'height' : '25px',
        'background' : 'url(/assets/images/rotateblack.gif)',
        'background-size' : 'cover',
      });
    });

   $('body').on('click', '.view-more-images', function(event) {
      event.preventDefault();
      var counter = parseInt($(this).attr('data-counter'), 10) + 1;
      $(this).attr('data-counter', counter);
      //$('a:contains("' + (counter - 1) + '")').attr('class', '');
      //$('a:contains("' + counter + '")').attr('class', 'active');

         var content = '';
         $.get('get_images.php', {'page': counter}, function(response) {
          if (response['images_page'].length) {
            $.each(response['images_page'], function(key, value) {      
                  content += 
                        '<div class="thumb">' +
                            '<a data-fancybox="gallery" href="' + value.path + '">' +
                                '<img class="img-fluid" src="' + value.path + '" alt="' + value.description + '">' +
                            '</a>' +
                          '<div class="caption">' +
                              '<span class="info">' + value.description + '</span>' +
                          '</div>' +
                        '</div>';
            });
            $('.row > .thumb:last').after(content);
          }
         }, 'json');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(this).offset().top
        }, 500);
   });

    $('#posts').on('click', function() {
              var content;

              $.ajax({
                    type: "POST",
                    url: "get_posts.php",
                    data: {'posts': true},
                    dataType: 'json',
                    success: function(data) {
                      //content = '<h1 class="h3 text-center my-4">Галерея</h1>';
                      if (!data['posts_page']) {
                        content = 
                            '<div class="not_posts">' +
                              '<span>Блог пока отсутствуют</span>' +
                            '</div>';
                      } else {
                          content = '<h1 class="h3 text-center my-4">Блог</h1>';
                          content += '<div class="row_posts">';             
                          $.each(data['posts_page'], function(key, value) {
                            /*content += 
                                '<div class="thumb">' +
                                  '<a data-fancybox="post" href="' + value.image + '">' +
                                    '<img class="img-fluid" src="' + value.image + '" alt="' + value.content + '">' +
                                  '</a>' +
                                    '<div class="caption">' +
                                        '<span class="info">' + value.content + '</span>' +
                                    '</div>' +
                                '</div>';*/
                              content += '<div class="well" style="margin-right: 5%">' +
                                            '<div class="media">' +
                                              '<a class="pull-left" href="#">' +
                                                '<img class="media-object" style="height: 150px" src="' + value.image + '">' +
                                              '</a>' +
                                              '<div class="media-body">' +
                                                '<h4 class="media-heading" style="margin-bottom: 3.5%">' + value.title + '</h4>' +
                                                  '<p>' + value.content + '</p>' +
                                                  '<ul class="list-inline list-unstyled" style="float: right; margin-top: 7%">' +
                                                    '<li>' +
                                                    '<span><i class="glyphicon glyphicon-calendar"></i>' + value.date + '</span>' +
                                                    '</li>' +
                                                  '</ul>' +
                                              '</div>' +
                                            '</div>' +
                                          '</div>';
                          });
                          var pageCount = Math.ceil(data['count_all'] / 6);
                          if (pageCount > 1) {
                            content += '<div class="pagination_posts">';
                                        //'<a href="http://sport.nmsk2/#">&laquo;</a>';
                            for (var i = 1; i <= pageCount; i++) {
                              if (i == 1) {
                                content += '<a href="#" class="active" data-type="num">' + i + '</a>';
                              } else {
                                content += '<a href="#" data-type="num">' + i + '</a>';
                              }
                            }                          
                            content += '<a class="next" href="#">&raquo;</a>' +
                                      '</div>';
                          }
                          content += '<div class="cssload-loader">' +
                                        '<div class="cssload-dot"></div>' +
                                        '<div class="cssload-dot"></div>' +
                                        '<div class="cssload-dot"></div>' +
                                      '</div>';
                          content += '</div>';

                          content += '<div class="view-more-posts" style="margin-left: auto; width: 220px; margin-right: auto; margin-top: -1%; display: block" data-counter="1">' +
                                        '<a href="#" onmouseover="this.style.textDecoration=\'none\'; this.style.color = \'#428bca\'">' +
                                          '<img src="assets/images/more.gif" style="margin-left: 10%">' +
                                          '<span style="margin-left: -2.5%; display: block;">Показать ещё</span>' +
                                        '</a>' +
                                     '</div>' +
                                  '</div>';

                          /*content += '<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a>';
                          content += footer;*/
                          /*content = '<h1 class="h3 text-center my-4">Галерея</h1>' +
                            '<div class="row">' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/1.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/1.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/2.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/2.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/3.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/3.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +
                                '<div class="col-lg-3 col-md-4 col-6 thumb">' +
                                    '<a data-fancybox="gallery" href="assets/images/gallery/4.jpg">' +
                                        '<img class="img-fluid" src="assets/images/gallery/4.jpg" alt="...">' +
                                    '</a>' +
                                '</div>' +  
                              '<div class="col-lg-3 col-md-4 col-6 thumb collapse">' +
                                  '<a data-fancybox="gallery" href="assets/images/gallery/5.jpg">' +
                                      '<img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">' +
                                  '</a>' +
                              '</div>' + 
                              '<div class="col-lg-3 col-md-4 col-6 thumb collapse">' +
                                  '<a data-fancybox="gallery" href="assets/images/gallery/5.jpg">' +
                                      '<img class="img-fluid" src="assets/images/gallery/5.jpg" alt="...">' +
                                  '</a>' +
                              '</div>' +
                            '</div>' +
                          '<a class="view more" href="javascript:void(0);" style="text-decoration: none">Показать больше</a>' + 
                            footer;*/
                      }

                      el.get(0).innerHTML = content;
                      $([document.documentElement, document.body]).animate({
                            scrollTop: $(".content").offset().top
                      }, 1000);
                    },
                    error: function(xhr, str) {
                     
                    }
              });
    });

    $('body').on('mouseover', '.view-more-posts', function() {
      $('.view-more-posts a img').css({
        'background' : 'url(/assets/images/button.gif)',
        //'transform' : 'rotate(380deg)',
        'background-size' : 'cover'
      });
    });

    $('body').on('mouseout', '.view-more-posts', function() {
      $('.view-more-posts a img').css({
        //'width' : '25px',
        //'height' : '25px',
        'background' : 'url(/assets/images/rotateblack.gif)',
        'background-size' : 'cover'
      });
    });

$('body').on('click', '.view-more-posts', function(event) {
      event.preventDefault();
      var counter = parseInt($(this).attr('data-counter'), 10) + 1;
      $(this).attr('data-counter', counter);
      //$('a:contains("' + (counter - 1) + '")').attr('class', '');
      //$('a:contains("' + counter + '")').attr('class', 'active');

         var content = '';
         $.get('get_posts.php', {'page': counter}, function(response) {
          if (response['posts_page'].length) {
            $.each(response['posts_page'], function(key, value) {      
                              content += '<div class="well" style="margin-right: 5%">' +
                                            '<div class="media">' +
                                              '<a class="pull-left" href="#">' +
                                                '<img class="media-object" style="height: 150px" src="' + value.image + '">' +
                                              '</a>' +
                                              '<div class="media-body">' +
                                                '<h4 class="media-heading" style="margin-bottom: 3.5%">' + value.title + '</h4>' +
                                                  '<p>' + value.content + '</p>' +
                                                  '<ul class="list-inline list-unstyled" style="float: right; margin-top: 7%">' +
                                                    '<li>' +
                                                    '<span><i class="glyphicon glyphicon-calendar"></i>' + value.date + '</span>' +
                                                    '</li>' +
                                                  '</ul>' +
                                              '</div>' +
                                            '</div>' +
                                          '</div>';
            });
            $('.row_posts > .well:last').after(content);
          }
         }, 'json');
        $([document.documentElement, document.body]).animate({
            scrollTop: $('.view-more-posts').offset().top
        }, 500);
   });

								    var	first_option_content = '<div id="topic_form" class="form">' +
								    					'<div class="post_message"></div>' +
						        						'<form method="POST" id="form_upload_post" name="form" action="javascript:void(null);">' +
													        '<div class="control-group">' +
													            '<div class="controls">' +
													              '<input type="hidden" id="owner_id" name="owner_id" class="owner_id" value="' + <? echo $_SESSION['id'] ?> + '">' +
													            '</div>' +
													            '<label class="control-label" for="title">Заголовок</label>' +
													            '<div class="controls">' +
													              '<input type="text" id="title" name="title" class="title" value="" placeholder="Заголовок">' +
													            '</div>' +
													            '<label class="control-label" for="content">Содержание</label>' +
													            '<div class="controls">' +
													              '<textarea id="content" name="content" class="content" value="" placeholder="Содержание">' +
													              '</textarea>' +
													            '</div>' +
													            '<label class="control-label" for="image">Изображение</label>' +
													            '<div class="controls">' +
													              '<input type="file" id="image" name="image" class="image">' +
													            '</div>' +
													        '</div>' +
													        '<div class="control-group" style="margin-top: 1%">' +
														        '<div class="controls">' + 
														            '<button type="submit" name="submit" class="btn btn-success">Добавить</button>' +
														        '</div>' +
													        '</div>' +
														'</form>' +
													  '</div>';	


                    var two_option_content = '<div id="topic_form" class="form">' +
                              '<div class="image_message"></div>' +
                                '<form method="POST" id="form_upload_image" name="form" action="javascript:void(null);">' +
                                  '<div class="control-group">' +
                                      '<div class="controls">' +
                                        '<input type="hidden" id="owner_id" name="owner_id" class="owner_id" value="' + <? echo $_SESSION['id'] ?> + '">' +
                                      '</div>' +
                                      '<label class="control-label" for="image">Изображение</label>' +
                                      '<div class="controls">' +
                                        '<input type="file" id="image" name="image" class="image">' +
                                      '</div>' +
                                      '<label class="control-label" for="description">Описание</label>' +
                                      '<div class="controls">' +
                                        '<textarea id="description" name="description" class="description" value="" placeholder="Описание">' +
                                        '</textarea>' +
                                      '</div>' +
                                  '</div>' +
                                  '<div class="control-group" style="margin-top: 1%">' +
                                    '<div class="controls">' + 
                                        '<button type="submit" name="submit" class="btn btn-success">Добавить</button>' +
                                    '</div>' +
                                  '</div>' +
                            '</form>' +
                            '</div>'; 

		//$('#form_singin').on('click', call_singin());
		$('#admin').on('click', function() { 
			$.ajax({
					type: "POST",
					url: "access_admin.php",
					success: function(data) {  
						if (data) {
							var content = '<div id="admin_menu">' +
											  '<div class="form-group">' +
												//'<label for="option">Опции:</label>' +
												'<select class="form-control" id="option" onchange="changeSelect(this)">' +
												   '<option value="1"> Добавить новость</option>' +
												   '<option value="2"> Добавить изображение в галерею</option>' +
												'</select>' +
											  '</div>' +
											  '<div id="admin_form" class="form">' +
												'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
												      '<div class="control-group">' +
												        '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
													        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
													    '</div>' +
												    '</div>' +
												'</form>' +
											  '</div>' +
										  '</div>' +
										  first_option_content;// +
									      //footer;	  
						} else {
							var content = '<div class="form">' +
											'<form method="POST" id="form_singin" name="form" action="javascript:void(null);" onclick="call_singin()">' +
											    '<div class="control-group">' +
											        '<label class="control-label" for="login">Логин</label>' +
											        '<div class="controls">' +
											            '<input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">' +
											        '</div>' +
											        '<label class="control-label" for="password">Пароль</label>' +
											        '<div class="controls">' +
											            '<input type="password" id="password" name="password" class="password" value="" placeholder="Пароль администратора">' +
											        '</div>' +
											        '<div class="controls" style="margin-top: 2%"> ' +
												        '<button type="submit" name="submit" class="btn btn-success">Войти</button>' +
												    '</div>' +
											    '</div>' +
											'</form>' +
										  '</div>';// +
										  //footer;
						}
						el.get(0).innerHTML = content;
					},
					error:  function(xhr, str) {
	  
					}
			});	
			$([document.documentElement, document.body]).animate({
		        scrollTop: $(".content").offset().top
		    }, 1000);
		});


				function call_singin() {
									$("#form_singin").validate({
							              errorElement: "div",
							              rules: {
							                login: "required",
							                password: "required",
							              },
							              messages: {
							                login: "Введите логин",
							                password: "Введите пароль",
							              },
							              submitHandler: function(form) { 
							                var msg = $("#form_singin").serialize(); 
							                $.ajax({
							                  type: "POST",
							                  url: "access_admin.php",
							                  data: msg,
							                  success: function(data) { 
								                  		if (data) {
								         				         		content = '<div id="admin_menu">' +
													  '<div class="form-group">' +
														//'<label for="option">Опции:</label>' +
														'<select class="form-control" id="option" onchange="changeSelect(this)">' +
														   '<option value="1"> Добавить новость</option>' +
														   '<option value="2"> Добавить изображение в галерею</option>' +
														'</select>' +
													  '</div>' +
													  '<div id="admin_form" class="form">' +
														'<form method="POST" id="form_logout" name="form" action="javascript:void(null);" onclick="call_logout()">' +
														      '<div class="control-group">' +
														        '<div class="controls"> ' +
															        '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' +
															    '</div>' +
														    '</div>' +
														'</form>' +
													  '</div>' +
												  '</div>' +
												  first_option_content;// +
											      //footer;
														} else {
															 var errorMessageStyleContent = '#error_log {' +
																						    'display: block;' +
																						    'position: absolute;' +
																						    'margin: 9.5% 0 5% 45%;' +
																						    'padding: 0 0.5% 0 0.5%;' +
																						    'background-color: red;' +
																						    'text-align: center;' +
																					    '}' +	
																					    '#error_message {' +
																						    'margin: 1;' +
																						    'color: white;' +				
																					    '};';
														var errorMessageStyle = document.createElement("style"); 
														errorMessageStyle.innerHTML = errorMessageStyleContent;
														var errorHideScriptContent = setTimeout(function() {
																			   			  $("#error_log").remove();
																			   			  $("#error_log_style").remove();
																			   			  $("#error_hide_script").remove();
																		   			  }, 3000);
														var errorHideScript = document.createElement("script");
														errorHideScript.innerHTML = errorHideScriptContent;
														var head = $("head")[0];
														head.append(errorMessageStyle);
														head.append(errorHideScript);
														var content = '<div id="error_log">' + 
																					'<span id="error_message">' +  
																				 		'Неверный логин или пароль' + 
																				 	'</span>' + 
																				'</div>' +				   	   
														'<div class="form">' +
															'<form method="POST" id="form_singin" name="form" action="javascript:void(null);" onclick="call_singin()">' +
														        '<div class="control-group">' +
														            '<label class="control-label" for="login">Логин</label>' +
														            '<div class="controls">' +
														              '<input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">' +
														            '</div>' +
														            '<label class="control-label" for="password">Пароль</label>' +
														            '<div class="controls">' +
														              '<input type="pssword" id="password" name="password" class="password" value="" placeholder="Пароль администратора">' +
														            '</div>' +
														            '<div class="controls" style="margin-top: 2%"> ' +
															            '<button type="submit" name="submit" class="btn btn-success">Войти</button>' +
															        '</div>' +
														        '</div>' +
														    '</form>' +
													    '</div>';// +
													    //footer;
														}
													el.get(0).innerHTML = content;
							                  },
							                  error:  function(xhr, str){

							                  }
							                });
							              }
						            });
						         };

		function call_logout() { 
									$("#form_logout").validate({ 
							              errorElement: "div", 
							              rules: { 
							                login: "required", 
							                password: "required", 
							              }, 
							              messages: { 
							                login: "Введите логин", 
							                password: "Введите пароль", 
							              }, 
							              submitHandler: function(form) {  
							                $.ajax({ 
							                  type: "POST", 
							                  url: "access_admin.php", 
							                  data: "logout", 
							                  success: function(data) { 
							                  		if (!data) {   				   	   
														var content = '<div class="form">' +
																			  '<form method="POST" id="form_singin" name="form" action="javascript:void(null);" onclick="call_singin()">' +
																		          '<div class="control-group">' +
																		              '<label class="control-label" for="login">Логин</label>' +
																		              '<div class="controls">' +
																		                '<input type="text" id="login" name="login" class="login" value="" placeholder="Логин администратора">' +
																		              '</div>' +
																		              '<label class="control-label" for="password">Пароль</label>' +
																		              '<div class="controls">' +
																		                '<input type="password" id="password"   name="password" class="password" value="" placeholder="Пароль администратора">' +
																		              '</div>' +
																		              '<div class="controls" style="margin-top: 2%"> ' +
																			              '<button type="submit" name="submit"   class="btn btn-success">Войти</button>' +
																			          '</div>' +
																		          '</div>' +
																		      '</form>' +
																	      '</div>';// +
																	      //footer; 
														} else {
															var content = '<div class="form">' +
																		   '<form method="POST" id="form_logout" name="form" action="javascript:void(null);" action="call_logout()">' +
																		       '<div class="control-group">' +
																		           '<div class="controls" style="margin: 2% 0 0 7.5%"> ' +
																			           '<button type="submit" name="submit" class="btn btn-success">Выйти</button>' + 
																			       '</div>' +
																		       '</div>' +
																		   '</form>' +
																	   '</div>';// +
																	   //footer; 
														}
													el.get(0).innerHTML = content; 
							                  }, 
							                  error:  function(xhr, str){ 
							              	  	
							                  } 
							                }); 
							              } 
						            }); 
						         };
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			var files;
			$('body').on('change', 'input[type=file]', function(){
				files = this.files;
			});

			$('body').on('click', '#form_upload_post', function( event ) {
			    $("#form_upload_post").validate({
				    errorElement: "div",
				    rules: {
				    	owner_id: "required",
				        title: "required",
				        content: "required",
				        image: "required"
				    },
				    messages: {
				        owner_id: "Ошибка определения владельца Блог, обратитесь к программисту!",
				        title: "Введите название",
				        content: "Введите содержание",
				        image: "Загрузите изображение"
				    },
				    submitHandler: function(form) {
				    	var data = new FormData();
				    	$.each(files, function(key, value){
							  data.append(key, value);
						  });
				    	var post = $("#form_upload_post").serializeArray();
				    	$.each(post, function(key, value){
				    		data.append(value['name'], value['value']);
						});	    	
				        $.ajax({
				            type: "POST",
				            url: "add_post.php",
				            data: data,
				            cache: false,
				            dataType: 'html',
				            processData: false,
				            contentType: false, 
				            success: function(data) {
				            	$('.post_message').show();
				                if (data) {
				                	$('.post_message').css({'background-color': '#82E0AA', 'color': 'white'});
				                	$('.post_message').text('Новость успешно добавлена');
				                } else {
				                	$('.post_message').css({'background-color': '#E74C3C', 'color': 'white'});
				                	$('.post_message').text('Новость не добавлена');
				                }
				                setTimeout(function(){
				                	$('.post_message').hide();
				                }, 3000);
				                $('#form_upload_post')[0].reset();
				            },
				            error: function(xhr, str) {
				            	$('#form_upload_post')[0].reset();
				            }
				        });
				    }
			    });
        });

        $('body').on('click', '#form_upload_image', function( event ) {
          $("#form_upload_image").validate({
            errorElement: "div",
            rules: {
              owner_id: "required",
                description: "required",
                image: "required"
            },
            messages: {
                owner_id: "Ошибка определения владельца Блог, обратитесь к программисту!",
                description: "Введите описание",
                image: "Загрузите изображение"
            },
            submitHandler: function(form) {
              var data = new FormData();
              $.each(files, function(key, value){
              data.append(key, value);
            });
              var imageData = $("#form_upload_image").serializeArray();
              $.each(imageData, function(key, value){
                data.append(value['name'], value['value']);
            });       
                $.ajax({
                    type: "POST",
                    url: "add_image.php",
                    data: data,
                    cache: false,
                    dataType: 'html',
                    processData: false,
                    contentType: false, 
                    success: function(data) {
                      $('.image_message').show();
                        if (data) {
                          $('.image_message').css({'background-color': '#82E0AA', 'color': 'white'});
                          $('.image_message').text('Изображение успешно добавлено');
                        } else {
                          $('.image_message').css({'background-color': '#E74C3C', 'color': 'white'});
                          $('.image_message').text('Изображение не добавлено');
                        }
                        setTimeout(function(){
                          $('.image_message').hide();
                        }, 3000);
                        $('#form_upload_image')[0].reset();
                    },
                    error: function(xhr, str) {
                      $('#form_upload_image')[0].reset();
                    }
                });
            }
          });
      });
 
      $(document.body).on('click', ".pagination_gallery > .prev, .pagination_gallery > .next", function(event) {

        var counter = parseInt($('a.active').text(), 10);

        if ($(this).hasClass('prev')) {
          counter--;
        } else if ($(this).hasClass('next')) {
          counter++;
        }

         event.preventDefault()
         var content;
         $.get('get_images.php', {'page': counter}, function(response) {
          if (response['images_page'].length) {
            content = '<h1 class="h3 text-center my-4">Галерея</h1>';
            content += '<div class="row">';
            $.each(response['images_page'], function(key, value) {      
                  content += 
                        '<div class="thumb">' +
                            '<a data-fancybox="gallery" href="' + value.path + '">' +
                                '<img class="img-fluid" src="' + value.path + '" alt="' + value.description + '">' +
                            '</a>' +
                          '<div class="caption">' +
                              '<span class="info">' + value.description + '</span>' +
                          '</div>' +
                        '</div>';
            });
            var pageCount = Math.ceil(response['count_all'] / 6);
            if (pageCount > 1) {
              content += '<div class="pagination_gallery">';
              if (counter > 1) {
                content += '<a class="prev" href="http://sport.nmsk2/#">&laquo;</a>';
              }

              for (var i = 1; i <= pageCount; i++) {
                  if (i == counter) {
                      content += '<a href="#" class="active" data-type="num">' + i + '</a>';
                  } else {
                      content += '<a href="#" data-type="num">' + i + '</a>';
                  }
              }      
              if (counter < pageCount) {                    
                content += '<a class="next" href="#">&raquo;</a>';
              }
              content += '</div>';
            }
            content += '</div>';
            content += '<div class="view-more-images" style="margin-left: auto; width: 150px; margin-right: auto; margin-top: 1%; display: block" data-counter="' + $('a[class="active"]').text() + '">' +
                                        '<a href="#" onmouseover="this.style.textDecoration=\'none\'; this.style.color = \'#428bca\'">' +
                                          '<img src="assets/images/more.gif" style="margin-left: 7%">' +
                                          '<span style="margin-left: -11%; display: block;">Показать ещё</span>' +
                                        '</a>' +
                                     '</div>' +
                                  '</div>';
            el.get(0).innerHTML = content;
          }
         }, 'json');
      })

      $(document.body).on('click', '.pagination_gallery > a[data-type="num"]', function(event) {
      
         var counter = parseInt(this.text);

         event.preventDefault()
         var content;
         $.get('get_images.php', {'page': counter}, function(response) {
          if (response['images_page'].length) {
            content = '<h1 class="h3 text-center my-4">Галерея</h1>';
            content += '<div class="row">';
            $.each(response['images_page'], function(key, value) {      
                  content += 
                        '<div class="thumb">' +
                            '<a data-fancybox="gallery" href="' + value.path + '">' +
                                '<img class="img-fluid" src="' + value.path + '" alt="' + value.description + '">' +
                            '</a>' +
                          '<div class="caption">' +
                              '<span class="info">' + value.description + '</span>' +
                          '</div>' +
                        '</div>';
            });
            var pageCount = Math.ceil(response['count_all'] / 6);
            if (pageCount > 1) {
              content += '<div class="pagination_gallery">';
              if (counter > 1) {
                content += '<a class="prev" href="http://sport.nmsk2/#">&laquo;</a>';
              }

              for (var i = 1; i <= pageCount; i++) {
                  if (i == counter) {
                      content += '<a href="#" class="active" data-type="num">' + i + '</a>';
                  } else {
                      content += '<a href="#" data-type="num">' + i + '</a>';
                  }
              }      
              if (counter < pageCount) {                    
                content += '<a class="next" href="#">&raquo;</a>';
              }
              content += '</div>';
            }

            content += '<div class="view-more-images" style="margin-left: auto; width: 300px; margin-right: auto; margin-top: 1%; display: block" data-counter="' + $('a[class="active"]').text() + '">' +
                                        '<a href="#" onmouseover="this.style.textDecoration=\'none\'; this.style.color = \'#428bca\'">' +
                                          '<img src="assets/images/more.gif" style="margin-left: 10%; width: 25px; height: 25px; background: url(/assets/images/rotateblack.gif) 0% 0% / cover;">' +
                                          '<span style="display: block;">Показать ещё</span>' +
                                        '</a>' +
                                     '</div>' +
                                  '</div>';
              content += '</div>';
            el.get(0).innerHTML = content;
          }
         }, 'json');
      });
//////////////////++++
      $(document.body).on('click', ".pagination_posts > .prev, .pagination_posts > .next", function(event) {

        var counter = parseInt($('a.active').text(), 10);

        if ($(this).hasClass('prev')) {
          counter--;
        } else if ($(this).hasClass('next')) {
          counter++;
        }

         event.preventDefault()
         var content;
         $.get('get_posts.php', {'page': counter}, function(response) {
          if (response['posts_page'].length) {
            content = '<h1 class="h3 text-center my-4">Блог</h1>';
            content += '<div class="row_posts">';
            $.each(response['posts_page'], function(key, value) {      
                              content += '<div class="well" style="margin-right: 5%">' +
                                            '<div class="media">' +
                                              '<a class="pull-left" href="#">' +
                                                '<img class="media-object" style="height: 150px" src="' + value.image + '">' +
                                              '</a>' +
                                              '<div class="media-body">' +
                                                '<h4 class="media-heading" style="margin-bottom: 3.5%">' + value.title + '</h4>' +
                                                  '<p>' + value.content + '</p>' +
                                                  '<ul class="list-inline list-unstyled" style="float: right; margin-top: 7%">' +
                                                    '<li>' +
                                                    '<span><i class="glyphicon glyphicon-calendar"></i>' + value.date + '</span>' +
                                                    '</li>' +
                                                  '</ul>' +
                                              '</div>' +
                                            '</div>' +
                                          '</div>';
            });
            var pageCount = Math.ceil(response['count_all'] / 6);
            if (pageCount > 1) {
              content += '<div class="pagination_posts">';
              if (counter > 1) {
                content += '<a class="prev" href="http://sport.nmsk2/#">&laquo;</a>';
              }

              for (var i = 1; i <= pageCount; i++) {
                  if (i == counter) {
                      content += '<a href="#" class="active" data-type="num">' + i + '</a>';
                  } else {
                      content += '<a href="#" data-type="num">' + i + '</a>';
                  }
              }      
              if (counter < pageCount) {                    
                content += '<a class="next" href="#">&raquo;</a>';
              }
              content += '</div>';
            }
            content += '</div>';
            content += '<div class="view-more-posts" style="margin-left: auto; width: 220px; margin-right: auto; margin-top: -1%; display: block" data-counter="' + $('a[class="active"]').text() + '">' +
                                        '<a href="#" onmouseover="this.style.textDecoration=\'none\'; this.style.color = \'#428bca\'">' +
                                          '<img src="assets/images/more.gif" style="margin-left: 10%">' +
                                          '<span style="margin-left: -2.5%; display: block;">Показать ещё</span>' +
                                        '</a>' +
                                     '</div>' +
                                  '</div>';
            el.get(0).innerHTML = content;
          }
         }, 'json');
      })

      $(document.body).on('click', '.pagination_posts > a[data-type="num"]', function(event) {
      
         var counter = parseInt(this.text);

         event.preventDefault()
         var content;
         $.get('get_posts.php', {'page': counter}, function(response) {
          if (response['posts_page'].length) {
            content = '<h1 class="h3 text-center my-4">Блог</h1>';
            content += '<div class="row_posts">';
            $.each(response['posts_page'], function(key, value) {      
                              content += '<div class="well" style="margin-right: 5%">' +
                                            '<div class="media">' +
                                              '<a class="pull-left" href="#">' +
                                                '<img class="media-object" style="height: 150px" src="' + value.image + '">' +
                                              '</a>' +
                                              '<div class="media-body">' +
                                                '<h4 class="media-heading" style="margin-bottom: 3.5%">' + value.title + '</h4>' +
                                                  '<p>' + value.content + '</p>' +
                                                  '<ul class="list-inline list-unstyled" style="float: right; margin-top: 7%">' +
                                                    '<li>' +
                                                    '<span><i class="glyphicon glyphicon-calendar"></i>' + value.date + '</span>' +
                                                    '</li>' +
                                                  '</ul>' +
                                              '</div>' +
                                            '</div>' +
                                          '</div>';
            });
            var pageCount = Math.ceil(response['count_all'] / 6);
            if (pageCount > 1) {
              content += '<div class="pagination_posts">';
              if (counter > 1) {
                content += '<a class="prev" href="http://sport.nmsk2/#">&laquo;</a>';
              }

              for (var i = 1; i <= pageCount; i++) {
                  if (i == counter) {
                      content += '<a href="#" class="active" data-type="num">' + i + '</a>';
                  } else {
                      content += '<a href="#" data-type="num">' + i + '</a>';
                  }
              }      
              if (counter < pageCount) {                    
                content += '<a class="next" href="#">&raquo;</a>';
              }
              content += '</div>';
            }
            content += '</div>';
            content += '<div class="view-more-posts" style="margin-left: auto; width: 220px; margin-right: auto; margin-top: -1%; display: block" data-counter="' + $('a[class="active"]').text() + '">' +
                                        '<a href="#" onmouseover="this.style.textDecoration=\'none\'; this.style.color = \'#428bca\'">' +
                                          '<img src="assets/images/more.gif" style="margin-left: 10%">' +
                                          '<span style="margin-left: -2.5%; display: block;">Показать ещё</span>' +
                                        '</a>' +
                                     '</div>' +
                                  '</div>';
            el.get(0).innerHTML = content;
          }
         }, 'json');
      });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	</script>
  </body>
</html>