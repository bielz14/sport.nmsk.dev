<?php
	/*$connect = mysqli_connect('localhost', 'root', '');
	$query = "SHOW DATABASES LIKE `sport`";
	if (!mysqli_query($connect, $query)) {
		$query = "CREATE DATABASE `sport`";
		if (!mysqli_query($connect, $query)) {
			echo 'Не создана БД для хранения таблиц<br><br>';
			exit;
		}
		echo 'БД для хранения таблиц создана<br><br>';
	}*/
	
	$connect = mysqli_connect('localhost', 'root', '', 'sport');
	$table = 'admin';
	$query = "create table " . $table . " (
		        user_id int(10) AUTO_INCREMENT,
		        login varchar(20) NOT NULL,
		        password varchar(200) NOT NULL,
		        date_of_registration varchar(50) NOT NULL,
		        PRIMARY KEY (user_id)
	)";
	if (mysqli_query($connect, $query)) {
	    echo 'Таблица `' . $table . '` успешно создана<br><br>';
	} else {
	    echo 'Таблица `' . $table . '` не была создана<br><br>';
	}

	$table = 'post';
    $query = "create table " . $table . " (
		        post_id int(10) AUTO_INCREMENT,
		        owner_id int(10) NOT NULL,
		        title varchar(100) NOT NULL,
		        content text NOT NULL,
		        image varchar(200) NOT NULL,
		        date_of_added varchar(50) NOT NULL,
		        PRIMARY KEY (post_id)
	)";
	if (mysqli_query($connect, $query)) {
	    echo 'Таблица `' . $table . '` успешно создана<br><br>';
	} else {
	    echo 'Таблица `' . $table . '` не была создана<br><br>';
	}

	$table = 'image';
    $query = "create table " . $table . " (
		        image_id int(10) AUTO_INCREMENT,
		        owner_id int(10) NOT NULL,
		        description text NOT NULL,
		        path varchar(200) NOT NULL,
		        date_of_added varchar(50) NOT NULL,
		        PRIMARY KEY (image_id)
	)";
	if (mysqli_query($connect, $query)) {
	    echo 'Таблица `' . $table . '` успешно создана<br><br>';
	} else {
	    echo 'Таблица `' . $table . '` не была создана<br><br>';
	}
		
?>